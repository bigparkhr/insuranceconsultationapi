package com.bg.insuranceconsultationapi.controller;

import com.bg.insuranceconsultationapi.model.InsuranceItem;
import com.bg.insuranceconsultationapi.model.InsuranceRequest;
import com.bg.insuranceconsultationapi.model.InsuranceResponse;
import com.bg.insuranceconsultationapi.service.InsuranceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/insurance")
public class InsuranceController {
    private final InsuranceService insuranceService;

    @PostMapping("/new")
    public String setInsurance (@RequestBody InsuranceRequest request) {
        insuranceService.setInsurance(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<InsuranceItem> getInsurances() {return insuranceService.getInsurances(); }

    @GetMapping("/detail/{id}")
    public InsuranceResponse getInsurance(@PathVariable long id) {
        return insuranceService.getInsurance(id);
    }
}
