package com.bg.insuranceconsultationapi.service;

import com.bg.insuranceconsultationapi.entity.Inserance;
import com.bg.insuranceconsultationapi.model.InsuranceItem;
import com.bg.insuranceconsultationapi.model.InsuranceRequest;
import com.bg.insuranceconsultationapi.model.InsuranceResponse;
import com.bg.insuranceconsultationapi.repository.InsuranceRepositiry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InsuranceService {
    private final InsuranceRepositiry insuranceRepositiry;

    public void setInsurance (InsuranceRequest request) {
        Inserance addData = new Inserance();

        addData.setDateMaker(LocalDate.now());
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setMobile(request.getMobile());
        addData.setGender(request.getGender());

        insuranceRepositiry.save(addData);
    }

    public List<InsuranceItem> getInsurances() {
        List<Inserance> orihinList = insuranceRepositiry.findAll();

        List<InsuranceItem> result = new LinkedList<>();

        for (Inserance inserance : orihinList) {
            InsuranceItem addItem = new InsuranceItem();
            addItem.setId(inserance.getId());
            addItem.setDateMaker(inserance.getDateMaker());
            addItem.setName(inserance.getName());
            addItem.setBirthDay(inserance.getBirthDay());
            addItem.setGender(inserance.getGender().getName());

            result.add(addItem);

        }

        return result;
    }

    public InsuranceResponse getInsurance(long id) {
        Inserance originData = insuranceRepositiry.findById(id).orElseThrow();

        InsuranceResponse response = new InsuranceResponse();
        response.setId(originData.getId());
        response.setDateMaker(originData.getDateMaker());
        response.setName(originData.getName());
        response.setBirthDay(originData.getBirthDay());
        response.setMobile(originData.getMobile());
        response.setGenderName(originData.getName());

        return response;
    }
}
