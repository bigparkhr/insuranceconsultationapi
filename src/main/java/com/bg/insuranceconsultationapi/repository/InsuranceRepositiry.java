package com.bg.insuranceconsultationapi.repository;

import com.bg.insuranceconsultationapi.entity.Inserance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceRepositiry extends JpaRepository<Inserance, Long> {
}
