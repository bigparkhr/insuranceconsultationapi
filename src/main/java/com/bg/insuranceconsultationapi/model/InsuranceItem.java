package com.bg.insuranceconsultationapi.model;

import com.bg.insuranceconsultationapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceItem {
    private Long id;
    private LocalDate dateMaker;
    private String name;
    private Integer birthDay;
    private String gender;

}
