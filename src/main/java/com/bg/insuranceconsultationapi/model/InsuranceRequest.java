package com.bg.insuranceconsultationapi.model;

import com.bg.insuranceconsultationapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsuranceRequest {
    private String name;
    private Integer birthDay;
    private String mobile;
    private Gender gender;
}
