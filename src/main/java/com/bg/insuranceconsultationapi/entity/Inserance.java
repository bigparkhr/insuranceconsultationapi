package com.bg.insuranceconsultationapi.entity;

import com.bg.insuranceconsultationapi.enums.Gender;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Inserance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateMaker;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer birthDay;

    @Column(nullable = false)
    private String mobile;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender gender;
}
